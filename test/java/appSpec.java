package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_outputCarID_whenValidRequest()
            throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"1\", \"vin\":\"AWE1ASD2QEW3ZXC45\", \"model\":\"mark 2\"  }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1")));
    }

    @Test
    public void should_outputBadRequestWhenVinLessThan17chars() throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"1\", \"vin\":\"1\", \"model\":\"mark 2\"  }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void should_outputBadRequestWhenVinHasSpacesThan17chars() throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"1\", \"vin\":\"AWE1AS D2QEW 3ZXQ\", \"model\":\"mark 2\"  }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_outputBadRequestWhenMarkLessThan4chars() throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"1\", \"vin\":\"AWE1ASD2QEW3ZXC45\", \"model\":\"mar\"  }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_failWhenCarIdIsEmptyThan4chars() throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"carID\":\"\", \"vin\":\"AWE1ASD2QEW3ZXC45\", \"model\":\"mar\"  }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
