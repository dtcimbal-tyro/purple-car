package app.domain.primitive;

import com.fasterxml.jackson.annotation.JsonRootName;

import javax.validation.constraints.NotNull;

public class CarID {

    @NotNull
    private Integer value;

    public CarID() {}

    public CarID(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
