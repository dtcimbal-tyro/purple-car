package app.domain.primitive;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Vin {

    @NotEmpty
    @Size(min = 17, max = 17)
    @Pattern(regexp = "[A-Z0-9]{17}")
    private String value;

    public Vin() {
    }

    public Vin(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
