package app.domain.primitive;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CarModel {

    @NotEmpty
    @Size(min = 3, max = 20)
    @Pattern(regexp = "[a-zA-Z0-9 ]{4,20}")
    private String value;

    public CarModel() {
    }

    public CarModel(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}
