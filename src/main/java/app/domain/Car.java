package app.domain;

import app.domain.primitive.CarID;
import app.domain.primitive.CarModel;
import app.domain.primitive.Vin;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@JsonDeserialize(using = CarDeserializer.class)
public class Car {
    @Valid
    @NotNull
    private CarID carID;

    @Valid
    @NotNull
    private Vin vin;

    @Valid
    @NotNull
    private CarModel model;
    //private String plateNo;
    //private File photo;

    public Car() {
        super();
    }

    public Car(CarID carID, Vin vin, CarModel model) {
        this.carID = carID;
        this.vin = vin;
        this.model = model;
    }

    public CarID getcarID() {
        return this.carID;
    }

    public Vin getvin() {
        return this.vin;
    }

    public CarModel getmodel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " " + this.vin.toString() + " " + this.model.toString();
    }

}
