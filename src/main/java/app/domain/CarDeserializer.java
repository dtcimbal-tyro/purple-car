package app.domain;

import app.domain.primitive.CarID;
import app.domain.primitive.CarModel;
import app.domain.primitive.Vin;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public class CarDeserializer extends JsonDeserializer<Car> {

    @Override
    public Car deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode root = p.readValueAsTree();
        CarID carID = new CarID(root.get("carID").asInt());
        Vin vin = new Vin(root.get("vin").asText());
        CarModel model = new CarModel(root.get("model").asText());
        return new Car(carID, vin, model);
    }

}

