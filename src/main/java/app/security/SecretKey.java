package app.security;

import java.util.Arrays;

public class SecretKey {

    private static final String SECRET_KEY_ENV = "SECRET_KEY";

    public static SecretKey create() {
        // The secret still will be available as System.getenv() at any point in time
        return new SecretKey(System.getenv().get(SECRET_KEY_ENV).getBytes());
    }

    private byte[] value;

    private SecretKey(byte[] value) {
        this.value = value.clone();
    }

    byte[] getValue() {
        if (this.value == null) throw new IllegalStateException("Secret key has been consumed");
        byte[] value = this.value.clone();

        // Erases array straight away
        Arrays.fill(this.value, (byte) 0);

        // Removes the pointer so the key will be collected by GC
        this.value = null;
        return value;
    }

}
