package app;

import app.domain.Car;
import app.exception.BadRequestException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class CarSaleController {

    @RequestMapping("/")
    String render() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String render(@Valid @RequestBody Car car, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getAllErrors().forEach(System.err::println);
            throw new BadRequestException(bindingResult.hashCode());
        }
        return "Added car: " + car.toString();
    }
}
