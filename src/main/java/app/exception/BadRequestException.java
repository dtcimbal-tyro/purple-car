package app.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "No such Order")
public class BadRequestException extends RuntimeException {

    public BadRequestException(int errorCode) {
        super(String.format("Bad Request, error: %d", errorCode));
    }
}
